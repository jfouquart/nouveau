image: alpine:latest

variables:
  FDO_UPSTREAM_REPO: drm/nouveau
  S3_HOST: s3.freedesktop.org
  CI_PRE_CLONE_SCRIPT: |-
          set -o xtrace
          /usr/bin/wget -q -O- ${CI_SERVER_URL}/nouveau/ci-scripts/-/raw/main/ci/download-git-cache.sh | sh -
          set +o xtrace

include:
  - project: 'freedesktop/ci-templates'
    file:
      - '/templates/ci-fairy.yml'

stages:
  - git-archive
  - checks
  - comment

# Generic rule to not run the job during scheduled pipelines
# ----------------------------------------------------------
.scheduled_pipelines-rules:
  rules: &ignore_scheduled_pipelines
    if: &is-scheduled-pipeline '$CI_PIPELINE_SOURCE == "schedule"'
    when: never

# YAML anchors for rule conditions
# --------------------------------
.rules-anchors:
  rules:
    # Pre-merge pipeline
    - if: &is-pre-merge '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME == $CI_COMMIT_REF_NAME'
      when: on_success

make git archive:
  extends:
    - .fdo.ci-fairy
  stage: git-archive
  rules:
    - if: *is-scheduled-pipeline
      when: on_success
  # ensure we are running on packet
  tags:
    - packet.net
  script:
    # make sure we have an unshallow repository
    - $(git rev-parse --is-shallow-repository) && git fetch --unshallow
    # fetch remote repos we want to sync with
    - git remote add mainline https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git || true
    - git remote add stable https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git || true
    - git remote add drm https://anongit.freedesktop.org/git/drm/drm.git || true
    - git fetch --all
    # push updated branches
    - git push ${CI_SERVER_PROTOCOL}://nouveau:${GIT_ACCESS_TOKEN}@${CI_SERVER_HOST}/${FDO_UPSTREAM_REPO}.git --tags refs/remotes/mainline/*:refs/heads/* refs/remotes/stable/*:refs/heads/* refs/remotes/drm/*:refs/heads/*
    # Compactify the .git directory
    - git gc
    # compress the current folder
    - tar -cvzf ../$CI_PROJECT_NAME.tar.gz .

    # login with the JWT token
    - ci-fairy s3cp ../$CI_PROJECT_NAME.tar.gz https://$S3_HOST/git-cache/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$CI_PROJECT_NAME.tar.gz

# checks
# ----------------

checkpatch:
  stage: checks
  rules:
    - if: *is-pre-merge
      when: always
  variables:
    ALLOWED_TYPES: "BRACES,COMMIT_LOG_LONG_LINE,COMMIT_MESSAGE,COMPLEX_MACRO,DEEP_INDENTATION,FILE_PATH_CHANGES,FUNCTION_ARGUMENTS,LINE_SPACING,LONG_LINE,LONG_LINE_STRING,SPACING,SUSPECT_CODE_INDENT,TRAILING_STATEMENTS,TYPO_SPELLING"
  script:
    - echo "something went wrong with the job, please check the log at $CI_JOB_URL" > check-patch-output.md
    - apk add git perl
    - |
      if ./scripts/checkpatch.pl -g $CI_MERGE_REQUEST_DIFF_BASE_SHA..HEAD --show-types --no-summary --color=always; then
        echo "no checkpatch errors found" > check-patch-output.md
        exit 0
      fi;
      echo "checkpatch output (full log here: $CI_JOB_URL)" > check-patch-output.md;
      echo "" >> check-patch-output.md;
      ./scripts/checkpatch.pl -g $CI_MERGE_REQUEST_DIFF_BASE_SHA..HEAD --show-types --ignore "$ALLOWED_TYPES" --no-summary --color=never > tmp.md || {
        printf '<details><summary>click here to see critical checkpatch issues which should be addressed</summary>\n\n```' >> check-patch-output.md;
        cat tmp.md >> check-patch-output.md;
        printf '```\n\n</details>\n\n' >> check-patch-output.md;
      }
      ./scripts/checkpatch.pl -g $CI_MERGE_REQUEST_DIFF_BASE_SHA..HEAD --show-types --types "$ALLOWED_TYPES" --no-summary --color=never > tmp.md || {
        printf '<details><summary>click here for other checkpatch warnings</summary>\n\n```' >> check-patch-output.md;
        cat tmp.md >> check-patch-output.md;
        printf '```\n\n</details>\n\n' >> check-patch-output.md;
      }
    # we don't want to always fail the job
    - ./scripts/checkpatch.pl -g $CI_MERGE_REQUEST_DIFF_BASE_SHA..HEAD --ignore "$ALLOWED_TYPES" >/dev/null || exit 1
  artifacts:
    name: check-patch-output
    when: always
    expire_in: 1 day
    paths:
    - check-patch-output.md

checkbuild:
  stage: checks
  rules:
    - if: *is-pre-merge
      when: always
  script:
    - echo "something went wrong with the job, please check the log at $CI_JOB_URL" > build-error-output.md
    - apk add bash bison diffutils elfutils-dev flex gcc git libc-dev linux-headers make xz
    - git worktree remove -f worktree/ || true
    - git worktree prune
    - git worktree add worktree/ HEAD
    - cd worktree/
    - make tinyconfig
    - >
      opts="
        CONFIG_WERROR=y
        CONFIG_64BIT=y
        CONFIG_X86_64=y
        CONFIG_IA32_EMULATION=y
        CONFIG_MODULES=y
        CONFIG_AGP=y
        CONFIG_PCI=y
        CONFIG_DRM=m
        CONFIG_NEW_LEDS=y
        CONFIG_STAGING=y
        CONFIG_MEMORY_HOTPLUG=y
        CONFIG_MEMORY_HOTREMOVE=y
        CONFIG_ZONE_DEVICE=y
        CONFIG_DEVICE_PRIVATE=y

        CONFIG_DRM_NOUVEAU=m

        CONFIG_ACPI=y
        CONFIG_ACPI_WMI=m
        CONFIG_ACPI_BUTTON=m
        CONFIG_DEBUG_FS=y
        CONFIG_COMPAT=y
        CONFIG_HWMON=y
        CONFIG_LEDS_CLASS=m
        CONFIG_DRM_NOUVEAU_SVM=y
        CONFIG_DRM_NOUVEAU_BACKLIGHT=y
        CONFIG_VGA_SWITCHEROO=y
      "
    - for o in ${opts}; do echo $o >> .config; done
    - make olddefconfig
    - cat .config
    - |
      for o in ${opts}; do
        if [ -z "$(grep ^$o .config)" ]; then
          echo "$o missing in .config" >> ../build-error-output.md
          exit 1
        fi
      done
    - rm .failed || true
    - git rebase $CI_MERGE_REQUEST_DIFF_BASE_SHA --exec "make -j${FDO_CI_CONCURRENT:-4} bzImage modules" || touch .failed
    - |
      if [ -f .failed ]; then
        git --no-pager show HEAD
        echo patch failed to compile
        make -j${FDO_CI_CONCURRENT:-4} bzImage modules --keep-going >/dev/null 2>&1 || true
        echo "compiler output (25 lines at most, full log here: $CI_JOB_URL)" > ../build-error-output.md
        echo '```' >> ../build-error-output.md
        make -j1 bzImage modules --keep-going 2>&1 | head -n 25 >> ../build-error-output.md || true
        echo '```' >> ../build-error-output.md
        echo "$(git rev-parse HEAD) failed to build" >> ../build-error-output.md
      else
        echo "no errors during build check" > ../build-error-output.md
      fi
    - cd ..
    - git worktree remove -f worktree/ || true
    - if [ -f .failed ]; then rm .failed; exit 1; fi
  artifacts:
    name: build-error-output
    when: always
    expire_in: 1 day
    paths:
    - build-error-output.md

comment result:
  stage: comment
  variables:
    CI_PRE_CLONE_SCRIPT: ""
    GIT_STRATEGY: none
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
  script:
    - apk add curl jq
    - COMMENT=$(mktemp)
    - echo "# MR pipeline result" >> $COMMENT
    - echo "## Checkpatch result" >> $COMMENT
    - cat check-patch-output.md >> $COMMENT
    - echo "## Test build result" >> $COMMENT
    - cat build-error-output.md >> $COMMENT
    - URL="$CI_API_V4_URL/projects/$CI_MERGE_REQUEST_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes"
    - |
      echo '{}' | jq  --rawfile data "$COMMENT" '{"body": $data}' |
      curl --request POST \
           --header "PRIVATE-TOKEN: $MR_API_TOKEN" \
           --header 'Content-Type: application/json' \
           ${URL} \
           --data @-
    - rm $COMMENT
